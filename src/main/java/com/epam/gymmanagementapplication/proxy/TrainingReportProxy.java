package com.epam.gymmanagementapplication.proxy;


import com.epam.gymmanagementapplication.dto.request.TrainingReportDTO;
import com.epam.gymmanagementapplication.dto.response.ReportDTO;
import com.epam.gymmanagementapplication.exception.ReportException;
import com.epam.gymmanagementapplication.faulttolerence.TrainingReportFaultTolerance;
import org.springframework.cloud.loadbalancer.annotation.LoadBalancerClient;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.context.annotation.Primary;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(name = "gym-report-management-app", fallback = TrainingReportFaultTolerance.class)
@LoadBalancerClient(name = "gym-report-management-app", configuration = TrainingReportFaultTolerance.class)
@Primary
public interface TrainingReportProxy {
    @PostMapping("/trainingReport/")
    ResponseEntity<Void> saveTrainingReport(@RequestBody TrainingReportDTO trainingReportDTO);
    @GetMapping("/trainingReport/")
    ResponseEntity<ReportDTO> getTraining(@RequestParam String username) throws ReportException;
}
