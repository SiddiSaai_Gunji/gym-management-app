package com.epam.gymmanagementapplication.proxy;


import com.epam.gymmanagementapplication.dto.request.LogDTO;
import com.epam.gymmanagementapplication.dto.request.NotificationDTO;
import com.epam.gymmanagementapplication.exception.GymNotificationException;
import com.epam.gymmanagementapplication.faulttolerence.TrainingNotifyFaultTolerance;
import jakarta.validation.Valid;
import org.springframework.cloud.loadbalancer.annotation.LoadBalancerClient;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.context.annotation.Primary;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

@FeignClient(name = "gym-notification-service", fallback = TrainingNotifyFaultTolerance.class)
@LoadBalancerClient(name = "gym-notification-service", configuration = TrainingNotifyFaultTolerance.class)
@Primary
public interface TrainingNotifyProxy {
    @PostMapping("/notify/")
    ResponseEntity<Void> sendReport(@RequestBody @Valid NotificationDTO notificationDTO) throws GymNotificationException;
    @GetMapping("/notify/")
    ResponseEntity<List<LogDTO>> getLogEntries();
}
