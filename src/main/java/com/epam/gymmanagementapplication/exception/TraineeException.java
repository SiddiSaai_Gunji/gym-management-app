package com.epam.gymmanagementapplication.exception;

public class TraineeException extends Exception{
    public TraineeException(String message) {
        super(message);
    }
}
