package com.epam.gymmanagementapplication.exception;

public class TrainingTypeException extends Exception{
    public TrainingTypeException(String message) {
        super(message);
    }
}
