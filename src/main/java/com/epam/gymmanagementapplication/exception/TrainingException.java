package com.epam.gymmanagementapplication.exception;

public class TrainingException extends Exception{
    public TrainingException(String message) {
        super(message);
    }
}
