package com.epam.gymmanagementapplication.exception;

public class GymNotificationException extends Exception{
    public GymNotificationException(String message) {
        super(message);
    }
}
