package com.epam.gymmanagementapplication.exception;

public class ReportException extends Exception{
    public ReportException(String message) {
        super(message);
    }
}
