package com.epam.gymmanagementapplication.exception;

public class TrainerException extends Exception{
    public TrainerException(String message) {
        super(message);
    }
}
