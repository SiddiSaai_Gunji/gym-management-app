package com.epam.gymmanagementapplication.exception;

public class UserException extends Exception{
    public UserException(String message) {
        super(message);
    }
}
