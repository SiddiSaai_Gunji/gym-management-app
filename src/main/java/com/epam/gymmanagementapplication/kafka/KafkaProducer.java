package com.epam.gymmanagementapplication.kafka;

import com.epam.gymmanagementapplication.dto.request.NotificationDTO;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class KafkaProducer {
    private final KafkaTemplate<String, String> sender;

    @Value("${topic.notify.channel}")
    private String NOTIFICATION_CHANNEL;


    public void sendNotification(NotificationDTO notificationDTO) throws JsonProcessingException {
        String sendingString = new ObjectMapper().writeValueAsString(notificationDTO);
        sender.send(NOTIFICATION_CHANNEL, sendingString);
    }

}
