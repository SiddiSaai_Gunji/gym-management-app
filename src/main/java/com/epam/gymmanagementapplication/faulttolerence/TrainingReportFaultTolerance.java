package com.epam.gymmanagementapplication.faulttolerence;

import com.epam.gymmanagementapplication.dto.request.TrainingReportDTO;
import com.epam.gymmanagementapplication.dto.response.ReportDTO;
import com.epam.gymmanagementapplication.exception.ReportException;
import com.epam.gymmanagementapplication.proxy.TrainingReportProxy;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class TrainingReportFaultTolerance implements TrainingReportProxy {
    @Override
    public ResponseEntity<Void> saveTrainingReport(TrainingReportDTO trainingReportDTO) {
        log.info("Entered Fault Tolerance Block of "+this.getClass().getName()+"  Class");
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @Override
    public ResponseEntity<ReportDTO> getTraining(String username) throws ReportException {
        log.info("Entered Fault Tolerance Block of "+this.getClass().getName()+"  Class");
        return new ResponseEntity<>(ReportDTO.builder().build(),
                HttpStatus.OK);
    }
}
