package com.epam.gymmanagementapplication.faulttolerence;


import com.epam.gymmanagementapplication.dto.request.LogDTO;
import com.epam.gymmanagementapplication.dto.request.NotificationDTO;
import com.epam.gymmanagementapplication.proxy.TrainingNotifyProxy;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
public class TrainingNotifyFaultTolerance implements TrainingNotifyProxy {


    @Override
    public ResponseEntity<Void> sendReport(NotificationDTO notificationDTO) {
        log.info("Entered Fault Tolerance Block of "+this.getClass().getName()+"  Class");
        return new ResponseEntity<>(HttpStatus.REQUEST_TIMEOUT);
    }

    @Override
    public ResponseEntity<List<LogDTO>> getLogEntries() {
        log.info("Entered Fault Tolerance Block of "+this.getClass().getName()+"  Class");
        return new ResponseEntity<>(HttpStatus.REQUEST_TIMEOUT);
    }
}
