package com.epam.gymmanagementapplication.controller;


import com.epam.gymmanagementapplication.dto.response.ReportDTO;
import com.epam.gymmanagementapplication.exception.ReportException;
import com.epam.gymmanagementapplication.kafka.KafkaProducer;
import com.epam.gymmanagementapplication.proxy.TrainingReportProxy;
import com.epam.gymmanagementapplication.util.StringConstants;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/gym/reports")
@RequiredArgsConstructor
@Slf4j
public class TrainingReportController {
    private final TrainingReportProxy trainingReportProxy;


    @GetMapping("/getReports")
    ResponseEntity<ReportDTO> getTrainingReports(@RequestParam String username) throws ReportException {
        log.info(StringConstants.ENTERED_CONTROLLER_MESSAGE.getValue(), "getTrainingReports", this.getClass().getName(), username);
        ResponseEntity<ReportDTO> response = trainingReportProxy.getTraining(username);
        log.info(StringConstants.EXITING_CONTROLLER_MESSAGE.getValue(), "getTrainingReports", this.getClass().getName());
        return response;
    }
}
