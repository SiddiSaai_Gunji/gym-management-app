package com.epam.gymmanagementapplication.controller;


import com.epam.gymmanagementapplication.dto.response.ReportDTO;
import com.epam.gymmanagementapplication.proxy.TrainingReportProxy;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.ResponseEntity;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class TrainingReportControllerTest {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    TrainingReportProxy trainingReportProxy;

    @Test
    void testGetTrainingReports() throws Exception {
        String username = "john123";
        ReportDTO reportDTO = new ReportDTO("siddhu", "sid", "sai", true, null);
        Mockito.when(trainingReportProxy.getTraining(username)).thenReturn(ResponseEntity.ok(reportDTO));
        mockMvc.perform(MockMvcRequestBuilders.get("/gym/reports/getReports").param("username", username)).andExpect(status().isOk());
    }

}
